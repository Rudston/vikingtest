<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_infos', function (Blueprint $table) {
            $table->id();
            $table->mediumText('weather')->nullable();
            $table->mediumText('hotels')->nullable();
            $table->mediumText('covid')->nullable();

            $table->unsignedBigInteger('search_parameter_id')->nullable();
            $table->foreign('search_parameter_id')->references('id')->on('search_parameters')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_infos');
    }
}
