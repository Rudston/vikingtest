<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CityInfosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CityInfosController::class, 'index'])->name('infos');
Route::post('/fetch', [CityInfosController::class, 'fetch'])->name('fetch');
Route::post('/store', [CityInfosController::class, 'store'])->name('store');

