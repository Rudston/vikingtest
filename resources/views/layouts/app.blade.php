<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>City Information Finder</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/flowbite@latest/dist/flowbite.min.css"/>
    <script src="https://unpkg.com/flowbite@1.3.4/dist/datepicker.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="application/javascript">
        $(document).ready(function(){
            $('#fetch-btn').prop("disabled",true);
        });
        $(document).on("change", '#city', function () {
            if ($(this).val() !== 'select') {
                $('#fetch-btn').prop("disabled",false);
            } else {
                $('#fetch-btn').prop("disabled",false);
            }
        });
        $(document).on("change", '#country', function () {
            var cityUrl = "{{ url('api/cities')}}";
            $.get(cityUrl,
                {country: $(this).val()},
                function (data) {
                    var model = $('#city');
                        model.empty();
                    if (data.data.length) {
                        model.append("<option>Select a city</option>");
                    }
                    $.each(data.data, function (index, element) {
                        model.append("<option value='" + element.id + "'>" + element.city + "</option>");
                    });
                }
            );
        });
    </script>
</head>
<body class="bg-gray-300">
@yield('content')
</body>

</html>
