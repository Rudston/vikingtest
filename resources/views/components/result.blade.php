
<div class="w-full h-full mb-6">
    <div class="columns-3 mt-4">
        <div class="py-2 uppercase text-xs font-bold text-gray-700">Weather {{$date}}</div>
        <div class="py-2 uppercase text-xs font-bold text-gray-700">Hotels</div>
        <div class="py-2 uppercase text-xs font-bold text-gray-700">Covid {{$covidDate}}</div>
    </div>
    <div class="columns-3 h-[auto] mb-4">
        <div class="shadow-inner p-2 h-[550px]">
            @if(!isset($weather['error']) && isset($weather))
                <ul>
                    @foreach($weather as $key => $value)
                        <li class="p-1 text-xs">{{$key}} : {{$value}} </li>
                    @endforeach
                </ul>
            @endif
            @if(isset($weather['error']))
                <span class="text-red-900 text-xs font-bold">{{$weather['error']}}</span>
            @endif
        </div>
        <div class="shadow-inner p-2 h-[550px]">
            @if(!isset($hotels['error']) && isset($hotels))
                <ul>
                @foreach($hotels as $value)
                   <li class="p-1 text-xs"> {{$value}} </li>
                @endforeach
                </ul>
            @endif
            @if(isset($hotels['error']))
                <span class="text-red-900 text-xs font-bold">{{$hotels['error']}}</span>
            @endif
        </div>
        <div class="shadow-inner p-2 h-[550px]">
            @if(!isset($covid['error']) && isset($covid))
                <ul>
                    @foreach($covid as $key => $value)
                        @if(in_array($key, ['cases', 'tests', 'deaths']))
                            <li class="p-1 my-2 text-xs">{!! nl2br(e($key . ": ".$value)) !!}</li>
                        @else
                            <li class="p-1 text-xs">{{ $key . ": ".$value }}</li>
                        @endif
                    @endforeach
                </ul>
            @endif
            @if(isset($covid['error']))
                <span class="text-red-900 text-xs font-bold">{{$covid['error']}}</span>
            @endif
        </div>
    </div>
    <form class="w-full py-2" action="{{ route('store') }}" method="post">
        @csrf
        <div class="w-full text-right">
            <button type="submit" class="text-white text-bold bg-green-600 opacity-1 p-2 rounded-xl">Export</button>
        </div>
    </form>
</div>
