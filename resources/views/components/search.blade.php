<div class="mb-4 w-full">
    <form class="w-full pt-4" action="{{ route('fetch') }}" method="post" id="search-frm">
        @csrf
        <div class="flex flex-wrap -mx-3 mb-2">
            <div class="w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state">
                    Country
                </label>
                <div class="relative">
                    <select
                        class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="country" name="country">
                        <option value="select">Select a country</option>
                        <option value="Denmark">Denmark</option>
                        <option value="France">France</option>
                        <option value="Germany">Germany</option>
                        <option value="Netherlands">Netherlands</option>
                        <option value="Norway">Norway</option>
                        <option value="Sweden">Sweden</option>
                    </select>
                </div>
            </div>
            <div class="w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-city">
                    City
                </label>
                <div class="relative">
                    <select
                        class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="city" name="city">
                    </select>
                </div>
            </div>
            <div class="w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                       for="datepicker">
                    Date
                </label>
                <div class="relative">
                    <input id="datepicker" datepicker type="text" name="date"
                           class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                           placeholder="Select a date"
                           datepicker-format="yyyy-mm-dd"
                           data-date="{{$today}}"
                           value="{{$today}}"
                    >
                </div>
            </div>
        </div>

        <div class="w-full text-right">
            <button id="fetch-btn" type="submit" class="text-white text-bold bg-green-600 opacity-1 p-2 rounded-xl">Fetch</button>
        </div>
    </form>
</div>
