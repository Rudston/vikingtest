@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-4 rounded-lg mt-12 text-center">
            <div class="w-full text-2xl text-bold">City Information Finder</div>
            <div class="h-2 w-full text-xs text-green-500 font-bold">{{ $success ?? '' }}</div>
        </div>
    </div>
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-3 rounded-lg mt-6">
            <div class="text-left">Search Parameters</div>
            <div class="flex justify-left">
                <x-search :today="$today"></x-search>
            </div>
        </div>
    </div>
    @if(isset($data))
        <div class="flex justify-center">
            <div class="w-8/12 bg-white p-3 rounded-lg mt-6">
                <div class="text-left">Information for {{$label}}</div>
                <div class="flex justify-left">
                    <x-result :data="$data"></x-result>
                </div>
            </div>
        </div>
    @endif
@endsection
