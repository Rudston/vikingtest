<?php

namespace App\View\Components;

use Illuminate\View\Component;

class search extends Component
{
    public $today;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($today)
    {
        $this->today = $today;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.search');
    }
}
