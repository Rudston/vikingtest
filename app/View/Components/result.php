<?php

namespace App\View\Components;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

class result extends Component
{
    public $data;

    public $weather;
    public $hotels;
    public $covid;
    public $date;
    public $covidDate;

    public function __construct($data)
    {
        $this->data = $data;
        $this->weather = $this->getWeatherDisplay($data['weather']);
        $this->hotels = $this->getHotelsDisplay($data['hotels']);
        $this->covid = $this->getCovidDisplay($data['covid']);
        $this->date = $data['date'];
    }

    private function getWeatherDisplay($weatherJson): Collection
    {
        $weatherArr = json_decode($weatherJson, true);
        if (isset($weatherArr['error'])) {
            return collect(['error' => implode(";", $weatherArr['error']['errors'])]);
        }
        $apiUsed = $weatherArr['api']; // history or forecast
        if ($apiUsed['type'] == 'future') {
            $daysDiff = $apiUsed['daysDiff']; // how many days past today
            if (isset($weatherArr['daily'][$daysDiff - 1])) {
                $daily = $weatherArr['daily'][$daysDiff - 1];
                $daily['description'] = ucfirst($daily['weather'][0]['description']);
                unset($daily['weather']);
                foreach ($daily as $key => $value) {
                    if (is_array($value)) {
                        $resultStr = "";
                        foreach ($value as $innerKey => $innerValue) {
                            $resultStr .= "$innerKey: $innerValue\n";
                        }
                        $daily[$key] = $resultStr;
                    } else {
                        $daily[$key] = $value;
                    }
                }
                return collect($daily);
            } else {
                return collect(['error' => 'No data returned.']);
            }
        } else {
            if (isset($weatherArr['current'])) {
                $current = $weatherArr['current'];
                $current['description'] = ucfirst($current['weather'][0]['description']);
                unset($current['weather']);
                return collect($current);
            } else {
                return collect(['error' => 'No data returned.']);
            }
        }
    }

    private function getHotelsDisplay($hotelsJson): Collection
    {
        $hotelsArr = json_decode($hotelsJson, true);
        if (isset($hotelsArr['error'])) {
            return collect(['error' => implode(";", $hotelsArr['error']['errors'])]);
        }
        $max = 10;
        $num = 0;
        $displayArr = [];
        if (isset($hotelsArr['suggestions'])) {
            foreach($hotelsArr['suggestions'] as $suggestionsArray) {
                $entities = $suggestionsArray['entities'];
                foreach($entities as $hotel) {
                    if ($num >= $max) {
                        break 2;
                    }
                    $displayArr[] = strip_tags($hotel["caption"]);
                    $num++;
                }
            }
            return collect($displayArr);
        } else {
            return collect(['error' => 'No data found.']);
        }
    }

    private function getCovidDisplay($covidJson): Collection
    {
        $covidArr = json_decode($covidJson, true);
        if (isset($covidArr['error'])) {
            return collect(['error' => implode(";", $covidArr['error']['errors'])]);
        }
        $displayArr = [];
        if (isset($covidArr['response'][0])) {
            $this->covidDate = $covidArr['covidDate'];
            $responseArr = $covidArr['response'][0]; // take first
            foreach ($responseArr as $key => $value) {
                if (is_array($value)) {
                    $resultStr = "";
                    foreach ($value as $innerKey => $innerValue) {
                        $resultStr .= "$innerKey: $innerValue\n";
                    }
                    $displayArr[$key] = $resultStr;
                } else {
                    $displayArr[$key] = $value;
                }
            }
            unset($displayArr['day']);
            unset($displayArr['time']);

            return collect($displayArr);
        } else {
            return collect(['error' => 'No data found. Note: no covid indicators for a future date are possible!']);
        }
    }


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.result');
    }
}
