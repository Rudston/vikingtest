<?php

namespace App\Models;

use App\Apis\CovidApi;
use App\Apis\HotelsApi;
use App\Apis\WeatherApi;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Log;

class SearchParameter extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'country',
        'city',
        'date',
        'lat',
        'lon'
    ];

    private $country;
    private $city;
    private $date;
    private $lat;
    private $lon;
    private $provided;

    public function __construct(array $attributes = [])
    {
        if (isset($attributes['_token'])) {
            unset($attributes['_token']);
        }
        if (isset($attributes['date'])) {
            $attributes['date'] = $attributes['date'] . " 00:00:00";
        }
        parent::__construct($attributes);
    }

    /**
     * Returns the has many relation for this item
     *
     * @return HasOne
     */
    public function cityInfos(): HasOne
    {
        return $this->hasOne(CityInfo::class);
    }

    /**
     * @return array
     */
    public function fetchInfos(): array
    {
        $weatherApi = new WeatherApi();
        $hotelsApi  = new HotelsApi();
        $covidApi  = new CovidApi();

        $infos['weather'] = $weatherApi->callApi($this);
        $infos['hotels'] = $hotelsApi->callApi($this);
        $infos['covid'] = $covidApi->callApi($this);
        $infos['search_parameter_id'] = $this->id;

        return $infos;
    }
}
