<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CityInfo extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'weather',
        'hotels',
        'covid',
    ];

    /**
     * The search parameter that this city info belongs to
     *
     * @return BelongsTo
     */
    public function searchParameter(): BelongsTo
    {
        return $this->belongsTo(SearchParameter::class);
    }
}
