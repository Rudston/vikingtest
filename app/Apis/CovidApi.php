<?php

namespace App\Apis;

use App\Models\SearchParameter;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

const COVID_URL = "https://covid-193.p.rapidapi.com/history";
const COVID_API_HOST = "covid-193.p.rapidapi.com";

class CovidApi
{
    private function validateParams(SearchParameter $searchParameter): array
    {
        $valid = true;
        $errors = [];
        $dateParamPresent = (bool)$searchParameter->date;
        $countryParamPresent = (bool)$searchParameter->country;
        if (!$dateParamPresent || !$countryParamPresent) {
            $valid = false;
            $errors[] = "Data invalid: date and country required";
        } else {
            try {
                $date = @Carbon::parse($searchParameter->date);
            } catch (InvalidFormatException $_) {
                $valid = false;
                $errors[] = "Invalid date supplied";
            }
            if(strlen($searchParameter->country) < 2) {
                $valid = false;
                $errors[] = "Invalid country supplied";
            }
        }
        return ['valid' => $valid, 'errors' => $errors];
    }

    public function callApi(SearchParameter $searchParameter)
    {
        $paramCheck = $this->validateParams($searchParameter);
        if ($paramCheck['valid']) {
            $day = new Carbon($searchParameter->date);
            $now = Carbon::now();
            $daysDiff = $day->diff($now)->days;
            if($day > $now && $daysDiff > 0) {
                // trying to get future covid: use today
                $day = $now;
                $covidDate = "today";
            } else {
                $covidDate = $day->diffForHumans();
            }

            $response = Http::timeout(20)->withHeaders([
                'x-rapidapi-host' => COVID_API_HOST,
                'x-rapidapi-key' => env('COVID_API_KEY')
            ])->get(COVID_URL, [
                'country' => strtolower($searchParameter->country),
                'day' => $day->toDateString()
            ]);

            if ($response->successful()) {
                $responseJson = $response->body();
                $responseArr = json_decode($responseJson, true);
                $responseArr['covidDate'] = $covidDate;
                return json_encode($responseArr);
            } else {
                $date = $searchParameter->date;
                $city = $searchParameter->city;
                Log::debug("Covid API call unsuccessful for city $city and date $date: " . $response->body());
                return json_encode(['error' => "Covid API call unsuccessful for city $city and date $date: " . $response->body()]);
            }
        } else {
            Log::debug("Covid API invalid data supplied: " .json_encode($paramCheck));
            return json_encode(['error' => $paramCheck]);
        }
    }
}
