<?php

namespace App\Apis;

use App\Models\SearchParameter;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

const WEATHER_URL_HISTORY = "https://api.openweathermap.org/data/2.5/onecall/timemachine";
const WEATHER_URL = "https://api.openweathermap.org/data/2.5/onecall";

class WeatherApi
{
    private function validateParams(SearchParameter $searchParameter): array
    {
        $valid = true;
        $errors = [];
        $dateParamPresent = (bool)$searchParameter->date;
        $latParamPresent = (bool)$searchParameter->lat;
        $lonParamPresent = (bool)$searchParameter->lon;
        $lat = number_format($searchParameter->lat, 4);
        $lon = number_format($searchParameter->lon, 4);
        if (!$dateParamPresent || !$latParamPresent || !$lonParamPresent) {
            $valid = false;
            $errors[] = "Data invalid: date, latitude and longitude required";
        } else {
            try {
                $date = @Carbon::parse($searchParameter->date);
            } catch (InvalidFormatException $_) {
                $valid = false;
                $errors[] = "Invalid date supplied";
            }
            if ($lat > 90 || $lat < -90) {
                $valid = false;
                $errors[] = "Invalid latitude supplied";
            }
            if ($lon> 180 || $lon < -180) {
                $valid = false;
                $errors[] = "Invalid longitude supplied";
            }
        }
        return ['valid' => $valid, 'errors' => $errors];
    }

    private function getQueryHistory(SearchParameter $searchParameter): string
    {
        $appId = env('WEATHER_API_KEY');
        $url = WEATHER_URL_HISTORY;
        $lat = $searchParameter->lat;
        $lon = $searchParameter->lon;
        $time = Carbon::createFromFormat('Y-m-d H:i:s', $searchParameter->date)->timestamp;

        return $url . "?lat=$lat&lon=$lon&dt=$time&appid=$appId";
    }
    private function getQuery(SearchParameter $searchParameter): string
    {
        $appId = env('WEATHER_API_KEY');
        $url = WEATHER_URL;
        $lat = $searchParameter->lat;
        $lon = $searchParameter->lon;
        $time = Carbon::createFromFormat('Y-m-d H:i:s', $searchParameter->date)->timestamp;

        return $url . "?lat=$lat&lon=$lon&dt=$time&appid=$appId";
    }
    public function callApi(SearchParameter $searchParameter)
    {
        $paramCheck = $this->validateParams($searchParameter);
        if ($paramCheck['valid']) {
            $now = Carbon::now();
            $searchParamDate = new Carbon($searchParameter->date);
            $daysDiff = $searchParamDate->diff($now)->days;

            if($searchParamDate <= $now && $daysDiff >= 0) {
                // historical
                $api = ['type' => 'history'];
                $query = $this->getQueryHistory($searchParameter);
            } else {
                // future from today
                $api = ['type' => 'future', 'daysDiff' => $daysDiff + 1];
                $query = $this->getQuery($searchParameter);
            }

            $response = Http::timeout(20)->get($query);
            if ($response->successful()) {
                $responseJson = $response->body();
                $responseArr = json_decode($responseJson, true);
                $responseArr['api'] = $api;
                return json_encode($responseArr);
            } else {
                $date = $searchParameter->date;
                $city = $searchParameter->city;
                Log::info("Weather API call unsuccessful for city $city and date $date: " . $response->body());
                return json_encode(['error' => "Weather API call unsuccessful for city $city and date $date: " . $response->body()]);
            }
        } else {
            Log::info("Weather API invalid data supplied: " .json_encode($paramCheck));
            return json_encode(['error' => $paramCheck]);
        }
    }
}
