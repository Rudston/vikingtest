<?php

namespace App\Apis;

use App\Models\SearchParameter;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

const HOTELS_URL = "https://hotels4.p.rapidapi.com/locations/v2/search";
const HOTELS_API_HOST = "hotels4.p.rapidapi.com";

class HotelsApi
{
    private function validateParams(SearchParameter $searchParameter): array
    {
        $valid = true;
        $errors = [];
        $cityParamPresent = (bool)$searchParameter->city;
        $countryParamPresent = (bool)$searchParameter->country;
        if (!$cityParamPresent || !$countryParamPresent) {
            $valid = false;
            $errors[] = "Data invalid: city and country required";
        } else {
            try {
                $date = @Carbon::parse($searchParameter->date);
            } catch (InvalidFormatException $_) {
                $valid = false;
                $errors[] = "Invalid date supplied";
            }
            if(strlen($searchParameter->country) < 2) {
                $valid = false;
                $errors[] = "Invalid country supplied";
            }
            if(strlen($searchParameter->city) < 2) {
                $valid = false;
                $errors[] = "Invalid city supplied";
            }
        }
        return ['valid' => $valid, 'errors' => $errors];
    }


    public function callApi(SearchParameter $searchParameter)
    {
        $paramCheck = $this->validateParams($searchParameter);
        if ($paramCheck['valid']) {
            $response = Http::timeout(20)->withHeaders([
                'x-rapidapi-host' => HOTELS_API_HOST,
                'x-rapidapi-key' => env('HOTELS_API_KEY')
            ])->get(HOTELS_URL, [
                'query' => $searchParameter->city.",".$searchParameter->country,
                'currency' => env('CURRENCY'),
                'locale' => env('LOCALE')
            ]);

            if ($response->successful()) {
                return $response->body();
            } else {
                $city = $searchParameter->city;
                Log::info("Hotels API call unsuccessful for city $city: " . $response->body());
                return json_encode(['error' => $response->body()]);
            }
        } else {
            Log::info("Hotels API invalid data supplied: " .json_encode($paramCheck));
            return json_encode(['error' => $paramCheck]);
        }
    }
}
