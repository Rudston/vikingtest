<?php

namespace App\Console\Commands;

use App\Models\SearchParameter;
use App\Models\CityInfo;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\Log;

class GetInfos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getinfos:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch infos for cities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $searchParameters = SearchParameter::where('provided', 1)->get();
        foreach ($searchParameters as $searchParameter) {
            $results = $searchParameter->fetchInfos();
            CityInfo::create($results);
        }
        return 0;
    }
}
