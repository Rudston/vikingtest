<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityResource;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Log;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        if($request->country !== "select") {
            $citiesOfCountry = City::where('country', $request->country)
                ->orderBy('id')
                ->take(20)
                ->get();
        } else {
            $citiesOfCountry = [];
        }
        return CityResource::collection($citiesOfCountry);
    }
}
