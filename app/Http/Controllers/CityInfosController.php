<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Support\Carbon;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Models\SearchParameter;
use App\Models\CityInfo;

class CityInfosController extends Controller
{
    /**
     * Store newly created CityInfo
     *
     */
    public function index()
    {
        $now = Carbon::now();
        return view('infos', ['today' => $now->format("Y-m-d")]);
    }
    /**
     * Fetch newly created CityInfo
     *
     */
    public function fetch(Request $request)
    {
        $this->validate($request, [
            'country' => 'required',
            'city' => 'required', // id
            'date' => 'required'
        ]);

        $params = [];
        $params['country'] = $request->input('country');
        $params['date'] = $request->input('date');
        $city = City::find($request->input('city'));
        $params['city'] = $city->city;
        $params['lat'] = (double)$city->lat;
        $params['lon'] = (double)$city->lon;

        $searchParameter = new SearchParameter($params);
        $data = $searchParameter->fetchInfos();

        // weather date display in result
        $dateDisplay = new Carbon($request->input('date'));
        $dateDisplay = $dateDisplay->diffForHumans();
        $data['date'] = strstr($dateDisplay, 'hours ago')? 'today': $dateDisplay;

        session(['searchParameters' => json_encode($params)]);

        $now = Carbon::now();
        $label = $city->city . ", " . $request->input('country');

        return view('infos', ['label' => $label, 'data' => $data, 'today' => $now->format("Y-m-d")]);
    }

    /**
     * Store infos for a city
     *
     */
    public function store()
    {
        $searchParametersJson = session()->pull('searchParameters', 'default');
        $searchParametersArr = json_decode($searchParametersJson, true);
        $searchParametersArr['provided'] = 0;
        $searchParameterObj = SearchParameter::create($searchParametersArr);
        $results = $searchParameterObj->fetchInfos();
        CityInfo::create($results);

        $now = Carbon::now();

        return view('infos', ['success' => "Your data was saved to the db", 'today' => $now->format("Y-m-d")]);
    }
}
