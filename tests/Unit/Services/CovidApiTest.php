<?php

namespace Tests\Unit\Services;

use App\Models\SearchParameter;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use App\Apis\CovidApi;

/**
 * Other tests would include:
 *
 * Test it returns current covid data for a date in the future
 */

class CovidApiTest extends TestCase
{
    /**
     * @return void
     */
    public function testItReturnsCurrentCovidDataWhenValidDataSupplied()
    {
        $searchParameter = new SearchParameter(['date' => Carbon::now()->toDateString(), 'country' => strtolower('Germany')]);
        $covidApi = new CovidApi();
        $result = $covidApi->callApi($searchParameter);
        $resultDecoded = json_decode($result, true);
        $this->assertTrue(is_array($resultDecoded['response'][0]));
        $this->assertTrue(is_array($resultDecoded['response'][0]['cases']));
        $this->assertTrue($resultDecoded['response'][0]['country'] == 'Germany');
    }

    /**
     * @return void
     */
    public function testItReturnsAnErrorWhenInValidDataSupplied()
    {
        $searchParameter = new SearchParameter(['date' => null, 'country' => null]);
        $covidApi = new CovidApi();
        $result = $covidApi->callApi($searchParameter);
        $resultDecoded = json_decode($result, true);

        $this->assertTrue(is_array($resultDecoded['error']));
        $this->assertTrue($resultDecoded['error']['errors'][0] == "Data invalid: date and country required");
    }
}
