<?php

namespace Tests\Unit\Services;

use App\Apis\HotelsApi;
use App\Models\SearchParameter;
use Tests\TestCase;

class HotelsApiTest extends TestCase
{
    /**
     * @return void
     */
    public function testItReturnsHotelInformationWhenValidDataSupplied()
    {
        $searchParameter = new SearchParameter(['country' => 'Germany', 'city' => 'Berlin']);
        $hotelsApi = new HotelsApi();
        $result = $hotelsApi->callApi($searchParameter);
        $resultDecoded = json_decode($result, true);
        $this->assertTrue(is_array($resultDecoded['suggestions']));
        $this->assertTrue($resultDecoded['term'] == "Berlin,Germany");
    }
}
