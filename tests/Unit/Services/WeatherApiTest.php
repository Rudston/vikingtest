<?php

namespace Tests\Unit\Services;

use App\Apis\WeatherApi;
use App\Models\SearchParameter;
use Illuminate\Support\Carbon;
use Tests\TestCase;

/**
 * Other tests would include:
 *
 * Test it returns past weather for a date in the past
 * Test it returns future weather for a future date
 *
 */

class WeatherApiTest extends TestCase
{
    /**
     * @return void
     */
    public function testItReturnsCurrentWeatherWhenValidDataSupplied()
    {
        $searchParameter = new SearchParameter(['date' => Carbon::now()->toDateString(), 'lat' => 52,5167, 'lon' => 13,3833]);
        $weatherApi = new WeatherApi();
        $result = $weatherApi->callApi($searchParameter);
        $resultDecoded = json_decode($result, true);

        $this->assertTrue(is_array($resultDecoded['current']));
        $this->assertTrue($resultDecoded['timezone'] == "Europe/Berlin");
    }
}
